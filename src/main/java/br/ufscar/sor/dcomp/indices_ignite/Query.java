package br.ufscar.sor.dcomp.indices_ignite;

import org.apache.ignite.*;
import org.apache.ignite.cache.affinity.*;
import org.apache.ignite.cache.query.*;
import org.apache.ignite.lang.*;
import java.io.*;
import java.util.Scanner;

public class Query {

	private static final String ORG_CACHE = "Organizations";

	public static void main(String[] args) throws IOException {
		Ignition.setClientMode(true);
		Scanner consulta = null;
		try (Ignite ignite = Ignition.start()) {
			IgniteCache<AffinityKey<IgniteUuid>, DadosPlanilha> cache = ignite.cache(null);
			System.out.println(cache.size());
			System.out.println("Qual consulta a ser carregada? : ");
			consulta = new Scanner(System.in);
			try(BufferedReader reader = new BufferedReader(new FileReader(""+consulta))){

				String line;
				while ((line = reader.readLine()) != null) {
					Scanner scanner = new Scanner(line);
					scanner.useDelimiter(" ");
					if(scanner.hasNext()){
						String buscaColuna = scanner.next();
						//String delimitador = System.getProperty("line.separator");
						String valor = scanner.next();
						String sql = null;
						if(buscaColuna.equals('1')){/*o valor da busca tem que estar nas duas colunas de dados?*/
							sql = "SELECT p.coluna_1 FROM DadosPlanilha p WHERE p.coluna_1 = " + valor + " ORDER BY p.coluna_1 ";
						}else if(buscaColuna.equals('2')){
							sql = "SELECT p.coluna_2 FROM DadosPlanilha p WHERE p.coluna_2 = " + valor + " ORDER BY p.coluna_1 ";
						}
						query(cache, sql);
					}else{
						System.err.println("Linha vazia!");
					}
					scanner.close();

				}
			}				
		}catch (FileNotFoundException ex){
			System.err.println("Arquivo "+consulta+" não encontrado!");
			System.exit(1);
		}
		catch (IOException ex){
			System.err.println("Erro ao ler linha do arquivo");
		}

	}

	private static void query(IgniteCache<AffinityKey<IgniteUuid>, DadosPlanilha> cache, String sql) {
		for (int i = 0; i < 3; i++) {
			long s = System.currentTimeMillis();
			cache.query(new SqlFieldsQuery(sql, false).setArgs(10)).getAll();
			long d = System.currentTimeMillis() - s;
			System.out.println("Tempo: " + d);
		}
	}
}