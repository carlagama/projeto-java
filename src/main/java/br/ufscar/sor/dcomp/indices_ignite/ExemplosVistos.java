package br.ufscar.sor.dcomp.indices_ignite;
/*package br.ufscar.sor.dcomp.indices_ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;


public class CacheApiExample {
	public static void main(String[] args) throws IgniteException{
		Ignition.setClientMode(true);
		
		try(Ignite ignite = Ignition.start("/home/carlagama/apache-ignite-fabric-1.7.0-bin/examples/config/example-ignite.xml")){
			CacheConfiguration<Long, DadosPlanilha> cfg = new CacheConfiguration<>();
			cfg.setCacheMode(CacheMode.REPLICATED);
			cfg.setName(DadosPlanilha.class.getName());
			//cfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
			cfg.setIndexedTypes(Long.class, DadosPlanilha.class);
			IgniteCache<Long, DadosPlanilha> cache = ignite.getOrCreateCache(cfg);
			
			//planilha = new Planilha();
			/*
			 *  
   person = new Person (1, "Name 1", "Lastname 1", new Embedded (10, "Embedded 10"),
                                 new Embedded (100, "Embedded 100"));
   cache.put (person.getId (), person);
   person = new Person (2, "Name 2", "Lastname 2", new Embedded (20, "Embedded 20"),
                                 new Embedded (200, "Embedded 200"));
   cache.put (person.getId (), person);
   person = new Person (3, "Name 3", "Lastname 3", new Embedded (20, "Embedded 30"),
                                 new Embedded (300, "Embedded 300"));
   cache.put (person.getId (), person); 
			 *
			 * 
			 * http://apache-ignite-users.70518.x6.nabble.com/Indexing-Querying-of-child-element-fields-td1704.html
			 * http://stackoverflow.com/questions/31983395/apache-ignite-indexing-performance
			 * http://apache-ignite-developers.2346864.n4.nabble.com/Indexing-primitive-types-td622.html
			 * https://github.com/apache/ignite/blob/master/examples/src/main/java/org/apache/ignite/examples/datagrid/starschema/DimStore.java
			 * https://github.com/apache/ignite/blob/master/examples/src/main/java/org/apache/ignite/examples/datagrid/store/CacheLoadOnlyStoreExample.java ***
			 * 
			
		}
	}
}	*/
