package br.ufscar.sor.dcomp.indices_ignite;

import org.apache.ignite.*;
import org.apache.ignite.cache.affinity.*;
import org.apache.ignite.configuration.*;
import org.springframework.util.StopWatch;

import java.io.*;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class CarregarArquivo {

	public static void main(String[] args) throws IOException {
		Ignition.setClientMode(true);
		String line;
		long i = 0;
		String[] coluna;
		DadosPlanilha linhaLida = new DadosPlanilha();


		try (Ignite ignite = Ignition.start("/home/carlagama/apache-ignite-fabric-1.7.0-bin/examples/config/example-ignite.xml")) {
			CacheConfiguration<AffinityKey<Long>, DadosPlanilha> cacheDadosConfig = new CacheConfiguration<>();
			cacheDadosConfig.setIndexedTypes(AffinityKey.class, DadosPlanilha.class);
			IgniteCache<AffinityKey<Long>, DadosPlanilha> dadosCache = ignite.getOrCreateCache(cacheDadosConfig);
			dadosCache.clear();

			System.out.println("Caches limpos.");

			StopWatch sw = new StopWatch();
			sw.start();

			try (IgniteDataStreamer<AffinityKey<Long>, DadosPlanilha> personStreamer = ignite.dataStreamer(null)) {
				try (BufferedReader reader = new BufferedReader(new FileReader("H2.dat"))) {
					while ((line = reader.readLine()) != null) {
						coluna = line.split(";");
						linhaLida.setColuna_1(coluna[0]);
						linhaLida.setColuna_2(coluna[1]);
						linhaLida.setId(i);
						System.out.println("1:: "+linhaLida.getColuna_1()+" 2:: "+linhaLida.getColuna_2());
						personStreamer.addData(new AffinityKey<>(i, i), linhaLida);
						i++;
					}
				}catch (FileNotFoundException ex){
					System.err.println("Arquivo não encontrado!");
					System.exit(1);
				}
				catch (IOException ex){
					System.err.println("Erro ao ler linha do arquivo");
				}
			}

			sw.stop();
			System.out.println("Dados carregados na memória\n");
			System.out.println("---------- RESUMO  ----------\n");
			System.out.println("Número de Registros	: " + dadosCache.size());
			System.out.println("Tempo	: "+sw);
			System.out.println("----------------------------------------------\n");
			
		}
	}
}