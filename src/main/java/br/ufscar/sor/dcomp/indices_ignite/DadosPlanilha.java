package br.ufscar.sor.dcomp.indices_ignite;

import org.apache.ignite.cache.query.annotations.QuerySqlField;
import java.io.*;

@SuppressWarnings("serial")
public class DadosPlanilha implements Serializable {
	
    @QuerySqlField(index = true)
    private long id;

    @QuerySqlField(index = true)
    private String coluna_1;

    @QuerySqlField(index = true)
    private String coluna_2;

    

	public DadosPlanilha(long id, String coluna_1, String coluna_2) {
        this.id = id;
        this.coluna_1 = coluna_1;
        this.coluna_2 = coluna_2;
    }

    public long getId() {
        return id;
    }

}